# Phil's Portfolio

Welcome to my virtual space where I store my portfolio project work.

## This repo

- Python
    - Python Principles Challenges - 24 Python coding challenges completed 4 July 2022
- JavaScript
    - Palindrome Checker
- Bash, because learning linux is fun!

## Other links

- tryhackme profile: [https://tryhackme.com/p/phil.wong](https://tryhackme.com/p/phil.wong)
